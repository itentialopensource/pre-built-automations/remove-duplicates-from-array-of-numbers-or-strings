
## 0.0.7 [01-31-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-numbers-or-strings!7

---

## 0.0.6 [06-14-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-numbers-or-strings!5

---

## 0.0.5 [12-03-2021]

* Changed Readme version to reflect current version

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-numbers-or-strings!4

---

## 0.0.4 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-numbers-or-strings!3

---

## 0.0.3 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/remove-duplicates-from-array-of-numbers-or-strings!2

---

## 0.0.2 [12-23-2020]

* Updated Readme.md

See merge request itentialopensource/pre-built-automations/staging/remove-duplicates-from-array-of-numbers-or-strings!1

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n
